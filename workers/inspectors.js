
const passport = require('passport')
const Inspector = require('../models/inspector')
const Ordrer = require('../models/evacOrder')
const _ = require('lodash')
var tesseract = require('node-tesseract');
const fs = require('fs');
var params = {
  input: '/Users/maxsimsmirnov/Desktop/Diplom/traffic_department/ocr/test.jpg',
  output: './out.txt',
  format: 'text'
};

module.exports = {
  initSignUp,
  initLogIning,
  endRegistration,
  adminLogin,
  getMe,
  formatedList,
  getApprovedOrders,
testOCR}

function initSignUp (req, res, next) {
  passport.authenticate('signup', (err, user, info) => {
    if (err) return res.json({error: err.toString()})
    req.logIn(user, (err) => {
      if (err) return res.json({error: err.toString()})
      return res.json({
        id: user._id,
        username: user.username
      })
    })
  })(req, res, next)
}
function endRegistration (req, res, next) {
  Inspector.findByIdAndUpdate({_id: req.body.id},
    {$set: {
        name: req.body.name,
        surname: req.body.surname,
        position: req.body.position,
        workPhone: req.body.workPhone,
        accessLevel: req.body.accessLevel
    }}, {new: true}, (err, doc) => {
      if (err) return res.json({error: err.toString()})
      res.json(doc)
    })
}

function initLogIning (req, res, next) {
  passport.authenticate('login', (err, user, info) => {
    if (err) return res.json({error: err.toString()})
    req.logIn(user, (err) => {
      if (err) return res.json({error: err.toString()})
      return res.json({
        id: user._id,
        username: user.username,
        fullname: user.name + ' ' + user.surname,
        position: user.position,
        workPhone: user.workPhone,
        accessLevel: user.accessLevel
      })
    })
  })(req, res, next)
}
function adminLogin (req, res, next) {
  passport.authenticate('login', (err, user, info) => {
    if (err) return res.json({error: err.toString()})
    Inspector.findOne({username: req.body.username}, (err, user) => {
      if (err) return res.send({error: err.toString()})
      if (!user) return res.json({error: 'Такого пользователя не существует!'})
      if (user.accessLevel != 5) {
        let err = 'Ваш уровень доступа не позволяет зайти сюда!'
        return res.json({error: err.toString()})
      } else {
        req.logIn(user, (err) => {
          if (err) return res.json({error: err.toString()})
          return res.json({
            id: user._id,
            username: user.username,
            name: user.name,
            middlename: user.middlename,
            surname: user.surname,
            position: user.position,
            workPhone: user.workPhone,
            accessLevel: user.accessLevel
          })
        })
      }
    })
  })(req, res, next)
}

function getMe (req, res, next) {
  let {username, _id, name, surname, middlename, position, workPhone, accessLevel} = req.user || {}
  let obj = Object.assign({error: null}, {username,_id,name,surname,middlename,position,workPhone,accessLevel})
  res.json(obj)
}

function formatedList (req, res, next) {
  let arr = []
  Inspector.find({}, (err, doc) => {
    if (err) return res.json({error: err.toString()})
    _.each(doc, item => {
      let struct = {
        ID: item._id,
        fullname: item.surname + ' ' + item.name + ' ' + item.middlename,
        position: item.position,
        workPhone: item.workPhone
      }
      arr.push(struct)
    })
    res.json(arr)
  })
}
function getApprovedOrders (req, res, next) {
  Ordrer.find({approved:true}).populate('evacInspector').exec((err, doc) => {
    if (err) return next({error: err})
    res.json(doc)
  })
}



function testOCR (req, res, next) {
  //let myImage = '/Users/maxsimsmirnov/Desktop/Diplom/traffic_department/ocr/' + req.body.picname;
  console.log(req.files);
  console.log(req.body);
  let myImage = req.files.file;
  let dir = './evacData/' + req.body.orderID;
  if(!fs.existsSync(dir)){
    fs.mkdirSync(dir);
  }
  // myImage.mv('./ocr/'+ myImage.name);
  // let picPath = './ocr/' + myImage.name;
  myImage.mv(dir + '/'+myImage.name,(err) => {
    if(err) {
      console.log(err);
      return res.json({error:err});
    }
        //fs.writeFileSync('./evacData/'+req.body.orderID+'/'+myImage.name,myImage)
    let picPath = dir + '/'+myImage.name;
    let options = {
      l: req.body.lang,
      psm: 3
  };
    tesseract.process(picPath,options,function(err, text) {
      if(err) {
          res.json({error: err});
      } else {
        text = text.replace(/^\s+|\s+$/g, ' ');
        console.log(text);
        if(text == " ") {
          res.json({almessage: 'Раcпознание не удалось, попробуйте еще раз!' })
        } else {
          res.json({ocrtext: text});
        }  
      }
  });
});
}
