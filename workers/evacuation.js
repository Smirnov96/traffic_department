module.exports = {
    initEvacuation,
    approveOrder,
    endEvacuation,
    testEvac
}
const _ = require('lodash');
const evacOrder = require('../models/evacOrder');
const inspector = require('../models/inspector');

const fs = require('fs')

function initEvacuation(req,res,next) {
    console.log(req.files)
    //fs.writeFile('log.txt',req.body.Photos)
    inspector.findById({_id: req.body.InpectorID},(err,insp) => {
        if(err) return next(err);
        if(insp) {
            if(!req.body.Photos) res.json('No files were uploaded!');
            let ph = JSON.parse(req.body.Photos);
            _.each(ph, item =>{
                console.log(item)
                let evacPhoto = item;
                evacPhoto.mv('./' + req.body.orderID + '/' + 'Photo №' + item.indexOf(item.name),err => {
                    if(err) return next(err);
                    next('suc');
                })
            })
             
            let newEvacuationOrder = new evacOrder({
                order_number: req.body.orderID,
                evac_time: new Date().toLocaleString(),
                evac_from: req.body.evacFrom,
                evac_to: req.body.evacTo,
                evacInspector: insp._id,
                violation: req.body.violation,
                carMark: req.body.carMark,
                carModel: req.body.carModel,
                carNumber: req.body.carNumber,
                //photosEvidence: req.body.Photos
            }).save((err,doc) => {
                if(err) return next(err);
                res.json(doc);
            });
        } else return next(new Error('Такого инспектора не существует!'))
    });  
};
function approveOrder(req,res,next) {
    evacOrder.findByIdAndUpdate({_id:req.body._id},{$set: {approved:req.body.approve}},{new:true},(err,order) => {
      if(err) return next({error:err});
      res.json(order);
    })
}
function endEvacuation(req,res,next) {
    evacOrder.findByIdAndUpdate({_id:req.body._id},{$set:{ended: req.body.endevac}},{new:true},(err,order) => {
        if(err) return next({error:err});
        res.json(order);
    });
};
function testEvac(req,res,next) {
        console.log(req.files);
        res.json(req.files)
        return 0;
}


