const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const expressSession = require('express-session')
const bCrypt = require('bcrypt-nodejs')

const Inspector = require('../models/inspector')

module.exports = function (passport) {
  passport.use('login', new LocalStrategy({
    passReqToCallback: true
  },
    (req, username, password, done) => {
      Inspector.findOne({username: username}, (err, user) => {
        if (err) return done(err)
        if (!user) {
          let err = 'Пользователя с таким логином не существует!'
          return done(err, false)
        }
        if (!isValidPassword(user, password)) {
          let err = 'Не верный пароль!'
          return done(err, false)
        }
        return done(null, user)
      })
    })
  )
  var isValidPassword = function (user, password) {
    return bCrypt.compareSync(password, user.password)
  }
}
