const Inspector = require('../models/inspector')
const login = require('./login');
const signup = require('./signup');

module.exports = function (passport) {
  passport.serializeUser((user, done) => {
    done(null, user._id)
  })

  passport.deserializeUser((id, done) => {
    Inspector.findById(id, (err, user) => {
      done(err, user)
    })
  });
  login(passport);
  signup(passport);
}
