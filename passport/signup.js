const LocalStrategy = require('passport-local').Strategy
const expressSession = require('express-session')
const bCrypt = require('bcrypt-nodejs')

const Insperctor = require('../models/inspector')

module.exports = function (passport) {
  passport.use('signup', new LocalStrategy({
    passReqToCallback: true
  },
    (req, username, password, done) => {
      findOrCreateInspector = () => {
        Insperctor.findOne({username: username}, (err, user) => {
          if (err) return done(err)
          if (user) {
            let err = 'Пользователь уже существует'
            return done(err, false)
          } else {
            let newInspector = new Insperctor()

            newInspector.username = username
            newInspector.password = createHash(password)

            newInspector.save(err => {
              if (err) return done(err)
              let message = 'Сохранено успешно'
              return done(null, newInspector)
            })
          }
        })
      }
      process.nextTick(findOrCreateInspector)
    })
  )
  var createHash = function (password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(100), null)
  }
}
