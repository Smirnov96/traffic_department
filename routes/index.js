var express = require('express')
var router = express.Router()
const { initEvacuation, testEvac } = require('../workers/evacuation')
const { initSignUp, initLogIning, endRegistration, adminLogin, getMe, formatedList, getApprovedOrders,testOCR } = require('../workers/inspectors')
const Inspector = require('../models/inspector')

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' })
})
router.post('/evacuation', initEvacuation)
router.post('/test', testEvac)
router.post('/signup', initSignUp)
router.post('/login', initLogIning)
router.post('/end_of_registration', endRegistration)
router.post('/admin/login', adminLogin)
router.get('/b04a32fcbbfddd89fe5b421e38b43519a66dd642', getMe)
router.get('/e8f49c72135c892e7667188f1dfecc53c02a84d3', formatedList)
router.get('/orders', getApprovedOrders);
router.post('/testocr',testOCR);

module.exports = router
