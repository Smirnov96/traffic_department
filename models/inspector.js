const mongoose = require('mongoose');

let Schema = mongoose.Schema({
    username: {
        type: String,
        unique: true
    },
    password: {
        type: String
    },
    name: {
        type: String
    },
    middlename: {
        type: String
    },
    surname: {
        type: String
    },
    position: {
        type: String
    },
    workPhone: {
        type: String
    },
    accessLevel: {
        type: Number
    }
},
{
    versionKey: false
});
let InspectorObject = {
    username: 'fio',
    password: '123',
    name: 'Максим',
    middlename: 'Викторович',
    surname: 'Смирнов',
    position: 'Лейтинант',
    workPhone: '+7(996)175-42-78',
    accessLevel: 5
}
module.exports = INSP = mongoose.model('Inspector',Schema);

INSP.find({},(err,insp) => {
    if(!insp.length) {
        let newInsp = INSP(InspectorObject);
        newInsp.save();
    }
})