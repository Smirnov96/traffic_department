const mongoose = require('mongoose');

let Schema = mongoose.Schema({
    order_number: {
        type: Number,
        unique: true
    },
    evac_time: {
        type: Date,
    },
    evac_from: {
        type: String
    },
    evac_to: {
        type: String
    },
    evacInspector: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Inspector'
    },
    violation: {
        type: String
    },
    carMark: {
        type: String
    },
    carModel: {
        type: String
    },
    carNumber: {
        type: String
    },
    photosEvidence : [
        {type: String}
    ],
    approved: {
        type: Boolean,
        default:false
    },
    ended: {
        type: Boolean,
        default: false
    }
},
{
    versionKey: false
});

module.exports = mongoose.model('EvacuationOrder',Schema);